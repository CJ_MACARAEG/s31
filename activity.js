 // 1. What directive is used by Node.js in loading the modules it needs?

 // ANSWER: Require directive is used to load a Node module(http) and store returned its instance(http) into its variable(http).

// 2. What Node.js module contains a method for server creation?

 // ANSWER: The http module contains the function to create the server

// 3. What is the method of the http object responsible for creating a server using Node.js?

 // ANSWER: createServer() method 

// 4. What method of the response object allows us to set status codes and content types?
	
 // ANSWER: reponse.writeHead()

// 5. Where will console.log() output its contents when run in Node.js?

 // ANSWER: In Terminal or GitBash Terminal

// 6. What property of the request object contains the address's endpoint?

 // ANSWER: response.end()